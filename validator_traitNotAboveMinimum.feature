Feature: Affiliations that do not allow a trait above it's minimum value
  Some affiliations allow a trait, but not above it's minimum value

  Scenario: A character does not have the restricted trait
    Given a new character
    When the character takes the affiliation: "Good Vision"
    Then the character should be "Valid"

  Scenario: A character has the trait at it's minimum value
    Given a new character
    When the character takes the affiliation: "Good Vision"
    And the character takes the trait: "Poor Vision" with -100 XP
    Then the character should be "Valid"

  Scenario: A character has the trait above it's minimum value
    Given a new character
    When the character takes the affiliation: "Good Vision"
    And the character takes the trait: "Poor Vision" with -200 XP
    Then the character should be "Invalid"